provider "aws" {
  region                      = var.region_instance
  access_key                  = "${var.AWS_ACCESS_KEY_ID}"
  secret_key                  = "${var.AWS_SECRET_ACCESS_KEY}"

}
resource "aws_instance" "dev" {
  ami                         = var.ami_code
  instance_type               = var.instance_type

  tags = {
    Name                      = "${var.instance_name}${format("%02d", count.index + 1)}"
  }

  count                       = "1"
  key_name                    = var.key_name
  subnet_id                   = var.subnet_id
  associate_public_ip_address = var.public_ip
  vpc_security_group_ids      = var.security_group

  root_block_device {
    volume_type               = var.volume_type
    volume_size               = var.volume_size
    delete_on_termination     = var.termination_delete
  }
}

resource "aws_route53_record" "dev" {
  zone_id = var.zone_id
  name    = var.domain_name
  type    = "A"
  ttl     = "300"
  records = "${aws_instance.dev[*].private_ip}"
}
