variable "region_instance" {
    description = "Instance region"
    type        = string
}

variable "AWS_SECRET_ACCESS_KEY" {
    description = "Secret key"
    type        = string
}

variable "AWS_ACCESS_KEY_ID" {
    description = "Access key"
    type        = string
}

variable "ami_code" {
    description = "Code for ami"
    type        = string
}

variable "instance_type" {
    description = "Type for instance"
    type        = string
}

variable "instance_name" {
    description = "Name tag for the EC2 instance"
    type        = string
}

variable "key_name" {
    description = "Master key"
    type        = string
}

variable "subnet_id" {
    description = "Subnet ID"
    type        = string
}

variable "public_ip" {
    description = "Associate public ip address"
    type        = string
}

variable "security_group" {
    description = "VPC security group ids"
    type        = list
}

variable "volume_type" {
    description = "Volume type"
    type        = string
}

variable "volume_size" {
    description = "Volume size"
    type        = string
}

variable "termination_delete" {
    description = "Delete on termination"
    type        = string
}

variable "zone_id" {
    description = "Zone id"
    type        = string
}

variable "domain_name" {
    description = "Route53 domain name"
    type        = string
}
