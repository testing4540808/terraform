terraform {
  backend "s3" {
    # Replace this with your bucket name!
    bucket         = "nobi-terraform-state"
    key            = "dev/mysql-qa/terraform.tfstate"
    region         = "ap-southeast-3"
    # Replace this with your DynamoDB table name!
    dynamodb_table = "terraform-up-and-running-locks"
    encrypt        = true
  }
}
