# ec2 variable
region_instance = "ap-southeast-3"

ami_code = "ami-0fc7a2028e57b4645"

instance_type = "t3.nano"

instance_name = "dev-mysql-qa-"

key_name = "nobi-master-key"

subnet_id  = "subnet-0c254890a741b9548"

public_ip = "true"

security_group = ["sg-00094c721b2fb7c3b","sg-046e245788d383a03"]

# volume variable
volume_type = "gp3"

volume_size = "10"

termination_delete = "true"

# domain variable
zone_id = "Z092505435ANHTOPTCH30"

domain_name = "dev-mysql-qa.nobi.internal"
